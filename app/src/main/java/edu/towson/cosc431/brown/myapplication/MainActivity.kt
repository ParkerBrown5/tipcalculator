package edu.towson.cosc431.brown.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*

class MainActivity : AppCompatActivity() {
    var tipPercent=0.0
    var checkAmount= 0.0
    private lateinit var calculateButton: Button
    private lateinit var input: TextView
    private lateinit var tipButton: RadioGroup
    private lateinit var output: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        calculateButton= findViewById(R.id.calculate)
        input = findViewById(R.id.checkAmount)
        tipButton= findViewById(R.id.tipButton)
        output= findViewById(R.id.result)

        calculateButton.setOnClickListener { calculateTip() }
    }
    fun calculateTip(){
        val id= tipButton.checkedRadioButtonId
        checkAmount= input.text.toString().toDouble()
        when(id){
            R.id.ten -> tipPercent=0.1
            R.id.twenty -> tipPercent=0.2
            R.id.thirty-> tipPercent=0.3
            else -> tipPercent=-1.0
        }
        if(tipPercent==-1.0){
            //this runs if a button isnt selected
            output.text="Please click a tip amount"
        }
        else{
            //this is a bunch of garbage to get 2 decimal points
            var tipHolder= tipPercent*checkAmount
            var totalHolder=tipHolder+checkAmount
            tipHolder=tipHolder*100
            totalHolder=totalHolder*100
            var tipSwitch=tipHolder.toInt()
            var totalSwitch=totalHolder.toInt()
            tipHolder=tipSwitch.toDouble()/100
            totalHolder=totalSwitch.toDouble()/100
            //end of garbage

            output.text="The tip amount is $tipHolder and the total is $totalHolder"
        }
    }
}